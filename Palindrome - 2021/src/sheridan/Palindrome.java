package sheridan;

public class Palindrome {
	
	public static boolean isPalindrome( String input ) {
		String newInput = input.trim().toLowerCase().replace(" ", "");
		for ( int i = 0 , j = newInput.length( ) - 1 ; i < j ; i++ , j-- ) {
			if ( newInput.charAt( i ) !=  newInput.charAt( j ) ) {
				return false;
			}
		}
		
		return true;
	}

	public static void main(String[] args) {
		
		System.out.println( "is 'anna' palindrome? " + Palindrome.isPalindrome( "anna" ) );
		System.out.println( "is 'race car' palindrome? " + Palindrome.isPalindrome( "race car" ) );
		System.out.println( "is 'taco cat' palindrome? " + Palindrome.isPalindrome( "taco cat" ) );
		System.out.println( "is 'this is not palindrome' palindrome? " + Palindrome.isPalindrome( "this is not palindrome" ) );

	}

}
